# Textbooks

- Not all textbooks here are processed by Turnip Boy, some have been donated to Turnip Boy from others, or found on the internet
- Turnip Boy asks that you support the textbook companies by purchasing their textbooks - these PDF copies are provided here for convenience for textbook companies who use proprietary reader applications
- Complaints under the Copyright Act 1968 (Cwth.) can be forwarded to Turnip Boy by opening an issue on [GitLab](https://gitlab.com/turnip-esa/textbooks/-/issues) 
- Turnip Boy loves you!

![Turnip Boy](https://m.media-amazon.com/images/I/51LV83omaKL._AC_SL1001_.jpg)
